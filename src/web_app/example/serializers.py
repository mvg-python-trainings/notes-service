from flask_restplus import fields

from web_app import api


example_item = api.model("ExampleItem", {
    "id": fields.Integer(attribute="id"),
    "title": fields.String(attribute="title"),
    "description": fields.String(attribute="description")
})


examples_list = api.model("ExamplesList", {
    "items": fields.List(fields.Nested(example_item), attribute="items")
})
