from flask_restplus import Namespace
from flask_restplus import Resource

from db import db, models

from web_app import api
from web_app.utils import parse_request_body
from .parsers import example_parser
from .serializers import example_item, examples_list


namespace = Namespace("example", description="Base info gathering endpoints")


@namespace.route("/examples/")
class ExamplesList(Resource):

    @api.marshal_with(examples_list)
    def get(self):
        return {"items": models.Example.query.all()}

    def post(self):
        request_body = example_parser.parse_args()
        title = request_body["title"]
        description = request_body["description"]
        example = models.Example(title=title, description=description)
        db.session.add(example)
        db.session.commit()
        return {}, 201


@namespace.route("/examples/<int:example_id>")
class Example(Resource):

    @api.marshal_with(example_item)
    def get(self, example_id):
        example = models.Example.query.get(example_id)
        if example is None:
            return {"error": "example not found"}, 404
        return {
            "title": example.title,
            "description": example.description
        }

    def delete(self, example_id):
        example = models.Example.query.get(example_id)
        if example is None:
            return {"error": "example not found"}, 404
        db.session.delete(example)
        db.session.commit()
        return {}, 200

    @api.marshal_with(example_item)
    @parse_request_body(example_item)
    def put(self, example_id):
        request_body = example_parser.parse_args()
        example = models.Example.query.get(example_id)
        if example is None:
            return {"error": "example not found"}, 404
        example.title = request_body["title"]
        example.description = request_body["description"]
        db.session.update(example)
        db.session.commit()
        return {
            "title": example.title,
            "description": example.description
        }
