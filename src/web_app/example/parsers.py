from flask_restplus.reqparse import RequestParser


example_parser = RequestParser()
example_parser.add_argument("title", type=str, required=True)
example_parser.add_argument("description", type=str, required=True)
