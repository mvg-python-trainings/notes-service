from flask import Flask

from .extensions import EXTENSIONS_LIST


def init_extensions(app: Flask) -> None:
    for extension in EXTENSIONS_LIST:
        assert getattr(extension, "init_app") is not None, f"'init_app' method not found in {extension}"
        extension.init_app(app)


def apply_error_handlers(app: Flask) -> None:
    pass
