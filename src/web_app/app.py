from flask import Flask
from flask_restplus import Api

from .initialization import init_extensions, apply_error_handlers
from configuration.app_config import APP_CONFIG


def create_app() -> Flask:
    app = Flask(__name__)
    app.secret_key = "app-example-secret-key"
    app.config.from_object(APP_CONFIG)
    init_extensions(app)
    apply_error_handlers(app)
    return app


def create_api(app: Flask) -> Api:
    api = Api(app=app, prefix=APP_CONFIG.API_URL_PREFIX)
    return api
