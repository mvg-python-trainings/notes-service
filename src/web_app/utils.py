from functools import wraps
from typing import Callable

from flask_restplus.reqparse import RequestParser

from web_app import api


def parse_request_body(parser: RequestParser, validate: bool = True) -> Callable:
    def factory(resource_request_handler) -> Callable:
        @api.expect(parser, validate)
        @wraps(resource_request_handler)
        def wrapper(*args, **kwargs):
            request_body = parser.parse_args()
            return resource_request_handler(*args, **kwargs, request_body=request_body)
        return wrapper
    return factory
