from .app import create_app, create_api


app = create_app()
api = create_api(app)

from web_app.example.views import namespace as example_namespace  # noqa


active_namespaces = [example_namespace]

for namespace in active_namespaces:
    api.add_namespace(namespace, path=namespace.path)
