import os

from .configuration import DevelopmentConfig, TestConfig, ProductionConfig


DEFAULT_CONFIG = "PRODUCTION"


APP_CONFIG = {
    "DEV": DevelopmentConfig,
    "TEST": TestConfig,
    "PRODUCTION": ProductionConfig,
}[os.environ.get("APP_ENV", DEFAULT_CONFIG)]()
