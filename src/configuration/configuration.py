import os
from enum import Enum

from constants import BASE_PATH


DEBUG = os.environ.get("DEBUG") or True
HOST = os.environ.get("APP_HOST") or "0.0.0.0"
PORT = os.environ.get("APP_PORT") or 8022
SECRET_KEY = os.environ.get("SECRET_KEY") or "app-example-secret-key"


class Database(Enum):
    ENGINE = os.environ.get("DB_ENGINE") or ""
    USER = os.environ.get("DB_USER") or ""
    PASSWORD = os.environ.get("DB_PASSWORD") or ""
    HOST = os.environ.get("DB_HOST") or ""
    PORT = os.environ.get("DB_PORT") or ""
    NAME = os.environ.get("DB_NAME") or ""

    @classmethod
    def retrieve_uri(cls) -> str:
        if all(i.value for i in cls):
            connection_uri_params = {
                "engine": cls.ENGINE.value,
                "user": cls.USER.value,
                "password": cls.PASSWORD.value,
                "host": cls.HOST.value,
                "port": cls.PORT.value,
                "name": cls.NAME.value,
            }
            return "{engine}://{user}:{password}@{host}:{port}/{name}".format(**connection_uri_params)
        db_name = cls.NAME.value or '_database'
        db_path = os.path.join(BASE_PATH, f"{db_name}.sqlite3")
        return f"sqlite:///{db_path}"


class BaseConfig:
    DEBUG = False
    APP_PORT = PORT
    SECRET_KEY = SECRET_KEY
    SQLALCHEMY_DATABASE_URI = Database.retrieve_uri()
    API_URL_PREFIX = "/api"
    ADMIN_URL_PREFIX = "/admin"


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = False


class TestConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"


class ProductionConfig(BaseConfig):
    DEBUG = False
    TESTING = False
