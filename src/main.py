from web_app import app
from configuration import configuration as conf


if __name__ == "__main__":
    app.run(host=conf.HOST, port=conf.PORT)
