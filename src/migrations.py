import os

from constants import BASE_PATH
from db import db
from flask_migrate import Migrate


migrate = Migrate(db=db, directory=os.path.join(BASE_PATH, "migrations"))
