from itertools import chain
from typing import Iterable, List


def unify_modules(modules: Iterable) -> List:
    return list(chain(*map(lambda i: i.__all__, modules)))
