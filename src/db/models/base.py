from ..extension import db


class SerializableModelMetaclass(db.Model.__class__):

    def __init__(self, name, bases, attrs):
        super().__init__(name, bases, attrs)
        columns = frozenset({name for name in attrs if isinstance(attrs[name], db.Column)})
        self._model_columns = columns


class SerializableModel(db.Model, metaclass=SerializableModelMetaclass):
    __abstract__ = True

    def as_dict(self, *, include=None, exclude=None):
        include = include or self._model_columns
        exclude = exclude or set()
        return {name: getattr(self, name) for name in include if name not in exclude}
