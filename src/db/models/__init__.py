from utils import unify_modules

from .example import *  # noqa


models_modules = [example]  # noqa


__all__ = unify_modules(models_modules)
