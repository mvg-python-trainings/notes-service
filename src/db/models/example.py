from ..extension import db
from datetime import datetime


__all__ = ["Example", "User", "Group", "Registration", "Task", "Category"]


class Example(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), index=True, unique=True)
    description = db.Column(db.String(512))

    def __repr__(self):
        return f"Example {self.title}"


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    surname = db.Column(db.String(255), nullable=False)
    birthdate = db.Column(db.DateTime())
    login = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)
    last_visit_date = db.Column(db.DateTime())
    registrations = db.relationship('Registration', backref='user',
                                    uselist=False)
    groups = db.relationship('Group', backref='user')
    tasks = db.relationship('Task', backref='user')

    group_id = db.Column(db.Integer(), db.ForeignKey('groups.id'))

    def __repr__(self):
        return f"User {self.name}"


class Group(db.Model):
    __tablename__ = 'groups'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)

    users = db.relationship('User', backref='group')

    def __repr__(self):
        return f"Group {self.name}"


class Registration(db.Model):
    __tablename__ = 'registrations'
    id = db.Column(db.Integer(), primary_key=True)
    validation_key = db.Column(db.Integer())
    validation_date_until = db.Column(db.DateTime())

    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))

    def __repr__(self):
        return f"Registration {self.validation_key}"


class Task(db.Model):
    __tablename__ = 'tasks'
    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    status = db.Column(db.String(255), nullable=False)
    create_date = db.Column(db.DateTime(), default=datetime.utcnow)
    due_date = db.Column(db.DateTime(), default=datetime.utcnow)

    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    category_id = db.Column(db.Integer(), db.ForeignKey('categories.id'))

    def __repr__(self):
        return f"Task {self.title}"


class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return f"Category {self.name}"
