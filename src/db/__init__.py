from utils import unify_modules
from . import extension
from . import models  # noqa
from .extension import *  # noqa


db_modules = [extension]

names_extension = ["models"]


__all__ = unify_modules(db_modules)
__all__.extend(names_extension)
