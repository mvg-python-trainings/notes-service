TOP_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: test-unit lint base-environment

test-unit:
	@export APP_ENV="TEST" && \
	export TOP_DIR="${TOP_DIR}" && \
	sh pytest.sh -vvv test/unittests/${test_module}

lint:
	flake8 ${TOP_DIR}

base-environment:
	pip install -r ${TOP_DIR}/requirements/requirements.txt
