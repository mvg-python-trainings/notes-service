#!/usr/bin/env bash

CURRENT_DIR=$(pwd)

PYTHONPATH=${CURRENT_DIR}/src python ${CURRENT_DIR}/src/main.py
