FROM python:3.8

WORKDIR /appmusic

COPY requirements/requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY src src
COPY test test
COPY Makefile Makefile
COPY pytest.sh pytest.sh
COPY docker-entrypoint.sh docker-entrypoint.sh

CMD ["/bin/sh", "docker-entrypoint.sh"]
